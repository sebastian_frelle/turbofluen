const cv = require('opencv');


    cv.readImage('qr_1490772606092.png', function(err, mat) {
        if(err) {
            console.log(err);
            return;
        }

        mat.convertGrayscale();
        mat.gaussianBlur([9, 9]);
        
        let circles = mat.houghCircles(0.5, 200);

        for(let i = 0; i < circles.length; i++) {
            let c = circles[i];
            mat.rectangle([c[0], c[1]], [2,2]);
        }

        mat.save('qr_1490772606092tratata.png');
    });
