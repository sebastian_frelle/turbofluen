const arDrone = require('ar-drone');
const cv = require('opencv');
const fs = require('fs');

const client = arDrone.createClient();

var pngStream = client.getPngStream();

var lastPng;
pngStream
.on('error', console.log)
.on('data', function(pngBuffer) {
    lastPng = pngBuffer;
    fs.writeFileSync('./test_new_ring/qr_' + Date.now() + '.png', lastPng);
});