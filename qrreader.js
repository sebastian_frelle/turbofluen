var fs = require('fs');
var QrCode = require('qrcode-reader');
var PNG = require('png-js');

var filename = process.argv[2];
var c = fs.readFileSync(filename);
var p = new PNG(c);

p.decode((data) => {

  var qr = new QrCode();
  
  qr.callback = (result) => {
    console.log("Result is: ");
    console.log(result);
  };

  qr.decode(p, data);
});