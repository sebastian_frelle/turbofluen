const arDrone = require('ar-drone');

function clamp(num, min, max) {
    if(num <= min) {
        return min;
    } else if(num >= max) {
        return max;
    } else {
        return num;
    }
}

const client = arDrone.createClient();

const drone = {
    xVel: 0,
    yVel: 0,
    zVel: 0,
    flying: false,
    rotationVel: 0,
    height: 0,
    client: client
};

drone.setXVel = function(vel) {
    this.xVel = clamp(vel, -1, 1);
    
    if(this.xVel > 0) {
        client.right(this.xVel);
    } else {
        client.left(Math.abs(this.xVel));
    }
};

drone.setYVel = function(vel) {
    this.yVel = clamp(vel, -1, 1);
    
    if(this.yVel > 0) {
        client.up(this.yVel);
    } else {
        client.down(Math.abs(this.yVel));
    }
};

drone.setZVel = function(vel) {
    this.zVel = clamp(vel, -1, 1);
    
    if(this.zVel > 0) {
        client.front(this.zVel);
    } else {
        client.back(Math.abs(this.zVel));
    }
};

drone.setRot = function(vel) {
    this.rotationVel = clamp(vel, -1, 1);
    
    if(this.rotationVel > 0) {
        client.clockwise(this.rotationVel);
    } else {
        client.counterClockwise(Math.abs(this.rotationVel));
    }
};

drone.stop = function() {
    this.xVel = 0;
    this.yVel = 0;
    this.zVel = 0;
    this.rotationVel = 0;
    
    client.stop();
};

drone.update = function() {
    setTimeout(drone.update, 1000 / 10);
};

module.exports = drone;