const cv = require('opencv');

var lowThresh = 0;
var highThresh = 100;
var nIters = 2;
var minArea = 1500;

var BLUE = [250, 0, 0]; // B, G, R
var RED = [0, 0, 255]; // B, G, R
var GREEN = [0, 255, 0]; // B, G, R
var YELLOW = [100, 100, 0]; 
var WHITE = [255, 255, 255]; // B, G, R

var lower_threshold = [0, 0, 0];
var upper_threshold = [80, 80, 80];


var fs = require('fs');
var QrCode = require('qrcode-reader');
var PNG = require('png-js');

cv.readImage('./QR_images/qr_1489576847348.png', function (err, im) {
    if (err) throw err;

    width = im.width()
    height = im.height()

    if (width < 1 || height < 1) throw new Error('Image has no size');

    var out = new cv.Matrix(height, width);
    var first = new cv.Matrix(height, width);

    im_canny = im.copy();
    im_canny.save('./result/original.png');

    //im_canny.brightness(0.5, 0.5);
    //im_canny.save('./result/bright.png');

    im_canny.inRange(lower_threshold, upper_threshold);
    im_canny.save('./result/2.png');

    contours = im_canny.findContours();

        for (i = 0; i < contours.size(); i++) {

            if (contours.area(i) < minArea) continue;

            var arcLength = contours.arcLength(i, true);
            contours.approxPolyDP(i, 0.01 * arcLength, true);

            switch (contours.cornerCount(i)) {
                case 4:
                    out.drawContour(contours, i, RED);
                     var points = [
                      contours.point(i, 0),
                      contours.point(i, 1),
                      contours.point(i, 2),
                      contours.point(i, 3)
                    ]
                    break;
                case 5:
                    out.drawContour(contours, i, RED);
                    break;
                default:
                   // out.drawContour(contours, i, WHITE);
            }
        }

    im_canny.canny(lowThresh, highThresh);
    im_canny.save('./result/3.png');

    contours = im_canny.findContours();

    for (i = 0; i < contours.size(); i++) {
        if (contours.area(i) < minArea) {
            var arcLength = contours.arcLength(i, true);
            contours.approxPolyDP(i, 0.01 * arcLength, true);

            switch (contours.cornerCount(i)) {
                case 4:
                    out.drawContour(contours, i, RED);
                    break;
                case 5:
                    out.drawContour(contours, i, RED);
                    break;
                default:
            }
        }
      
        im_canny.dilate(1);
        im_canny.save('./result/4.png');
        im_canny.erode(1);
        im_canny.save('./result/5.png');

        contours = im_canny.findContours();

        for (i = 0; i < contours.size(); i++) {

            if (contours.area(i) < minArea) continue;

            var arcLength = contours.arcLength(i, true);
            contours.approxPolyDP(i, 0.01 * arcLength, true);

            switch (contours.cornerCount(i)) {
                case 4:
                    out.drawContour(contours, i, RED);
                     var points = [
                      contours.point(i, 0),
                      contours.point(i, 1),
                      contours.point(i, 2),
                      contours.point(i, 3)
                    ]
                    break;
                case 5:
                    out.drawContour(contours, i, RED);
                    break;
                default:
                    //out.drawContour(contours, i, WHITE);
            }
        }
    
  }
    out.save('./result/detect_shapes.png');

    if(points != null){

    var punkt1;
    var punkt2;
    var punkt3;
    var punkt4;

    var xGennemsnit = (points[3].x+points[2].x+points[1].x+points[0].x)/4;
    var yGennemsnit = (points[3].y+points[2].y+points[1].y+points[0].y)/4;

    for (var i = 0; i < points.length; i++) {
      if(points[i].x < xGennemsnit){
        if(points[i].y < yGennemsnit){
          punkt1 = [points[i].x, points[i].y];
        }
        else{
          punkt2 = [points[i].x, points[i].y];
        }
      }
      else{
        if(points[i].y < yGennemsnit){
          punkt4 = [points[i].x, points[i].y];
        }
        else{
          punkt3 = [points[i].x, points[i].y];
        }
      }
    }


      out.line(punkt1, punkt2, GREEN);
      out.line(punkt3, punkt4, YELLOW);

      var y1 = Math.sqrt((punkt1[0] - punkt2[0])*(punkt1[0]-punkt2[0])+(punkt1[1]-punkt2[1])*(punkt1[1]-punkt2[1]));
      var y2 = Math.sqrt((punkt3[0] - punkt4[0])*(punkt3[0]-punkt4[0])+(punkt3[1]-punkt4[1])*(punkt3[1]-punkt4[1]));

    
    console.log('Grøn er ' + y1 + ' og blå er ' + y2);

    if(y1 < y2){
      if((y1/y2*100)>95) {
        console.log(y1/y2*100);
        console.log('lige');
        
        

        var filename = process.argv[2];
        var c = fs.readFileSync('./QR_images/qr_1489576847348.png');
        var p = new PNG(c);

        p.decode((data) => {

          var qr = new QrCode();
          
          qr.callback = (result) => {
            console.log("Result is: ");
            console.log(result);
          };

          qr.decode(p, data);
        });
      }

      else{
        console.log('venstre');
      }
    }
    else{
      if((y2/y1*100)>95) {
        console.log(y2/y1*100);
        console.log('lige');
        
        var filename = process.argv[2];
        var c = fs.readFileSync('./QR_images/qr_1489576845551.png');
        var p = new PNG(c);

        p.decode((data) => {

        var qr = new QrCode();
  
        qr.callback = (result) => {
          console.log("Result is: ");
          console.log(result);
        };

  qr.decode(p, data);
});
      }
      else{
        console.log('højre');
      }
    }

    }

    out.save('./result/hough.png');
    console.log('Image saved to ./result/detect-shapes.png');
});