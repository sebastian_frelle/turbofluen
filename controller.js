const keypress = require('keypress');
const circlepos = require('./circlepos.js')
const cv = require('opencv');

const drone = require('./drone.js');
let pngStream = drone.client.getPngStream();

function printVel(dir, label, vel) {
    console.log(dir + '\t' + label + ': ' + vel.toFixed(1));
}

drone.client.on('navdata', function (navdata) {
    if (!navdata.demo) {
        return;
    }

    if (navdata.demo.altitudeMeters) {
        drone.height = navdata.demo.altitudeMeters;
    }
});

// circles lel
let circleCallback = function (circles) {
    if (circles.length == 0) {
        console.log("no circles found");
        return;
    }

    // get first circle
    let c = circles[0];

    let horpos = (c.horizontal > 0) ? 'left' : 'right';
    let verpos = (c.vertical > 0) ? 'above' : 'below';

    console.log(`circle is ${horpos} and ${verpos} relative to center`);
};

let s = new cv.ImageStream();
pngStream.pipe(s);

s.once('data', () => {
    setInterval(() => {
        circlepos.findCircles(s, circleCallback);
    }, 1000);
});

keypress(process.stdin);

process.stdin.on('keypress', function (ch, key) {
    if (key) {
        if (key.name === 'space') {
            if (!drone.flying) {
                drone.client.takeoff();
                console.log('taking off');
            } else {
                drone.stop();
                drone.client.land();
                console.log('landing');
            }

            drone.flying = !drone.flying;
        } else if (key.name === 'c') {
            if (key.ctrl) {
                if (!drone.flying) {
                    console.log('bai');
                    process.exit();
                } else {
                    console.log('land before quitting');
                }
            }
        } else if (!drone.flying) {
            console.log('you must take off first');
            return;
        }

        switch (key.name) {
            case 'up':
                if (key.shift) {
                    drone.setYVel(drone.yVel + 0.1);
                    printVel('up', 'yVel', drone.yVel);
                } else {
                    drone.setZVel(drone.zVel + 0.1);
                    printVel('forward', 'zVel', drone.zVel);
                }
                break;

            case 'down':
                if (key.shift) {
                    drone.setYVel(drone.yVel - 0.1);
                    printVel('down', 'yVel', drone.yVel);
                } else {
                    drone.setZVel(drone.zVel - 0.1);
                    printVel('back', 'zVel', drone.zVel);
                }
                break;

            case 'right':
                if (key.shift) {
                    drone.setXVel(drone.xVel + 0.1);
                    printVel('strafe right', 'xVel', drone.xVel);
                } else {
                    drone.setRot(drone.rotationVel + 0.1);
                    printVel('turn right', 'rotVel', drone.rotationVel);
                }
                break;

            case 'left':
                if (key.shift) {
                    drone.setXVel(drone.xVel - 0.1);
                    printVel('strafe left', 'xVel', drone.xVel);
                } else {
                    drone.setRot(drone.rotationVel - 0.1);
                    printVel('turn left', 'rotVel', drone.rotationVel);
                }
                break;

            case 's':
                drone.stop();
                console.log('stopping');
                break;
            case 'k':
                console.log('calibrating');
                drone.client.calibrate(0);
                break;
        }
    }
});

drone.update();

process.stdin.setRawMode(true);
process.stdin.resume();