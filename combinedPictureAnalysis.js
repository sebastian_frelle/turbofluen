const arDrone = require('ar-drone');
const cv = require('opencv');
const fs = require('fs');
const QrCode = require('qrcode-reader');
const PNG = require('png-js');

var circlepos = require('./circlepos.js');

var lowThresh = 0;
var highThresh = 100;
var minArea = 1500;

var lower_threshold = [0, 0, 0];
var upper_threshold = [80, 80, 80];

var currentPortNumber = 0;

const client = arDrone.createClient();

var pngStream = client.getPngStream();

var s = new cv.ImageStream();

s.on('error', function(err) {
  console.log("An error occurred: " + err);
  }).on('data', function(im) {
    width = im.width();
    height = im.height();

    im_canny = im.copy();
    im_canny.save('./result/qr.png');

    //billedebehandling
    im_canny.inRange(lower_threshold, upper_threshold);
    im_canny.canny(lowThresh, highThresh);
    im_canny.dilate(1); 
    im_canny.erode(1); 

    contours = im_canny.findContours(); 

    var points = detectSquares(contours);

    if (points) {
      var sides = calculateSqaureSidesSize(points);
    
    if (sides) {

    if(sides[0] < sides[1]){
      if((sides[0]/sides[1]*100)>95) {

        readQR('./result/qr.png', function(ret) {
          console.log(ret);
          if(ret == -1) {
            console.log('wall found');
          }
          else if(ret == currentPortNumber) {

            // search for circles 

            // flyv 50 cm op 
            console.log('flyt mig 50 cm op');
            console.log('leder efter cirkel')

            
            // find circle * 5 


            // flyv 20 cm tilbage






          }
          else{
            //search for new qr


          }
        });
      }
      else{
        //console.log('venstre');
      }
    }
    else{
      if((sides[1]/sides[0]*100)>95) {

        readQR('./result/qr.png', function(ret) {
          console.log(ret);
          if(ret == 0) {
            console.log('wallie');
          }
          else if(ret == currentPortNumber) {

            // search for circles 
          }
          else{
            //search for new qr
          }
        });
        
      }
      else{
        //console.log('højre');
      }
    }
  }
    else{
        //ingen firkanter 
    }
  }
  
});
client.getPngStream().pipe(s);




function detectSquares (contours) {
  for (i = 0; i < contours.size(); i++){

    if (contours.area(i) < minArea){
      continue;
    }
        
    var arcLength = contours.arcLength(i, true);
    contours.approxPolyDP(i, 0.01 * arcLength, true);

    if (contours.cornerCount(i)==4){
      points = [
        contours.point(i, 0),
        contours.point(i, 1),
        contours.point(i, 2),
        contours.point(i, 3)
      ]
      return points;
    }


    else{
      return null;
    }
  }
}

function calculateSqaureSidesSize (points) {
  var punkt1, punkt2, punkt3, punkt4;
  var xGennemsnit = (points[3].x+points[2].x+points[1].x+points[0].x)/4;
  var yGennemsnit = (points[3].y+points[2].y+points[1].y+points[0].y)/4;

  for (var i = 0; i < points.length; i++) {
    if(points[i].x <= xGennemsnit){
      if(points[i].y <= yGennemsnit){
        punkt1 = [points[i].x, points[i].y];
      }
      else{
        punkt2 = [points[i].x, points[i].y];
      }
    }
    else{
      if(points[i].y <= yGennemsnit){
        punkt4 = [points[i].x, points[i].y];
      }
      else{
        punkt3 = [points[i].x, points[i].y];
      }
    }
  }

  if(!punkt1 || !punkt2 || !punkt3 || !punkt4){
    return;
  }

  var y1 = Math.sqrt((punkt1[0] - punkt2[0])*(punkt1[0]-punkt2[0])+(punkt1[1]-punkt2[1])*(punkt1[1]-punkt2[1]));
  var y2 = Math.sqrt((punkt3[0] - punkt4[0])*(punkt3[0]-punkt4[0])+(punkt3[1]-punkt4[1])*(punkt3[1]-punkt4[1]));  

  return [y1, y2];
}




function readQR (qrfile, done) {

  var c = fs.readFileSync(qrfile);
  var p = new PNG(c);

  p.decode((data) => {

    var qr = new QrCode();
    
    qr.callback = (result, err) => {
      if(result) {
        console.log("Result is: ");
        console.log(result);

        if(result.charAt(0).localeCompare('W')==0){
          console.log('wall');
          done(0)
          return;
        }
        else {
          done(result.charAt(3));
          return;
        }

        
      }
      else {
        console.log(err);
        return 'None';
      }
    };

    qr.decode(p, data);
  });

}
