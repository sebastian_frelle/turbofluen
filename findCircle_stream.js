const arDrone = require('ar-drone');
const cv = require('opencv');
const fs = require('fs');

const client = arDrone.createClient();

var pngStream = client.getPngStream();

var noOfCircles = 0;

var s = new cv.ImageStream();

s
.on('error', function(err) {
    console.log("An error occurred: " + err);
})
.on('data', function(matrix) {
    matrix.convertGrayscale();
    matrix.gaussianBlur([9, 9]);

    let circles = matrix.houghCircles(0.5, 200);

    if (circles.length !== noOfCircles && circles.length > 0) {
        noOfCircles = circles.length;
        client.animateLeds('blinkRed', 10, 1);
    }

    console.log("Number of circles registered: " + circles.length);
});

client.getPngStream().pipe(s);