const arDrone = require('ar-drone');
const cv = require('opencv');
const fs = require('fs');

const client = arDrone.createClient();

var pngStream = client.getPngStream();

var lastPng;
pngStream
.on('error', console.log)
.on('data', function(pngBuffer) {
    lastPng = pngBuffer;
    fs.writeFileSync('ring.png', lastPng);
    readImage();
});

function readImage() {
    cv.readImage('ring.png', function(err, mat) {
        if (err) {
            console.log(err);
            return;
        }

        mat.convertGrayscale();
        mat.gaussianBlur([9, 9]);

        let circles = mat.houghCircles(0.5, 200);

        for(let i = 0; i < circles.length; i++) {
            let c = circles[i];
            mat.rectangle([c[0], c[1]], [2,2]);
        }

    // mat.save('./test.png');
    mat.save('./test_images/' + Date.now() + '.png');
});
}