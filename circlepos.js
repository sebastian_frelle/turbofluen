const cv = require('opencv');

// horizontal:  >0: circle left of center,  <0: circle right of center
// vertical:    >0: circle above center,    <0: circle below center
let Circle = function(horizontal, vertical) {
    this.horizontal = horizontal;
    this.vertical = vertical;
};

exports.findCircles = (stream, callback) => {
    stream.once('data', (mat) => {
        console.log('cirker');
        mat.convertGrayscale();
        mat.gaussianBlur([9, 9]);
        
        let circlesRead = mat.houghCircles(0.5, 200);
        let circles = [];
        
        circlesRead.forEach((c) => {
            let circle = new Circle(mat.width()/2 - c[0], mat.height()/2 - c[1]);
            circles.push(circle);
        });
        
        callback(circles);
    });
};