const keypress = require('keypress');
const arDrone = require('ar-drone');

const client = arDrone.createClient();

const drone = {
    xVel: 0,
    yVel: 0,
    zVel: 0,
    flying: false,
    rotationVel: 0,
    height: 0,
    battery: 0
};

drone.updateYVel = function() {
    if(this.yVel > 0) {
        client.up(this.yVel);
    } else {
        client.down(Math.abs(this.yVel));
    }
};

drone.updateZVel = function() {
    if(this.zVel > 0) {
        client.front(this.zVel);
    } else {
        client.back(Math.abs(this.zVel));
    }
};

drone.updateXVel = function() {
    if(this.xVel > 0) {
        client.right(this.xVel);
    } else {
        client.left(Math.abs(this.xVel));
    }
};

drone.takeoff = function() {
    if(!drone.flying) {
        client.takeoff();
        drone.flying = true;
    }
};

drone.land = function() {
    if(drone.flying) {
        client.land();
        drone.flying = false;
        drone.stop();
    }
};

drone.update = function() {
    setTimeout(drone.update, 1000 / 30);
    console.log("Current height: " + drone.height);

    if(drone.height >= 1) {
        drone.stop();
        console.log("Drone stopped----");
    } else {
        drone.yVel = 0.2;
        drone.updateYVel();
        console.log('up', 'yVel', drone.yVel);
    }
};

drone.updateRot = function() {
    if(this.rotationVel > 0) {
        client.clockwise(this.rotationVel);
    } else {
        client.counterClockwise(Math.abs(this.rotationVel));
    }
};

drone.stop = function() {
    this.xVel = 0;
    this.yVel = 0;
    this.zVel = 0;
    this.rotationVel = 0;
    
    client.stop();
};

function printVel(dir, label, vel) {
    console.log(dir + '\t' + label + ': ' + vel.toFixed(1));
}

client.on('navdata', function (navdata) {
    if (!navdata.demo) {
        return;
    }
    
    if (navdata.demo.altitudeMeters) {
        drone.height = navdata.demo.altitudeMeters;
    }

    drone.battery = navdata.demo.batteryPercentage;
});

keypress(process.stdin);

process.stdin.on('keypress', function(ch, key) {
    if(key) {
        if(key.name === 'space') {
            if(!drone.flying) {
                drone.takeoff();
                console.log('taking off');
            } else {
                drone.land();
                console.log('landing');
            }
        } else if(key.name === 'c') {
            if(key.ctrl) {
                if(!drone.flying) {
                    console.log('bai');
                    process.exit();
                } else {
                    console.log('land before quitting');
                }
            }
        } else if(key.name === 'b') {
            console.log('battery: ' + drone.battery);
        } else if(!drone.flying) {
            console.log('you must take off first');
            return;
        }
        
        switch(key.name) {
            case 'up':
            if(key.shift) {
                drone.yVel += 0.1;
                drone.yVel = Math.min(drone.yVel, 1);
                drone.updateYVel();
                printVel('up', 'yVel', drone.yVel);
            } else {
                drone.zVel += 0.1;
                drone.zVel = Math.min(drone.zVel, 1);
                drone.updateZVel();
                printVel('forward', 'zVel', drone.zVel);
            }
            break;
            
            case 'down':
            if(key.shift) {
                drone.yVel -= 0.1;
                drone.yVel = Math.max(drone.yVel, -1);
                drone.updateYVel();
                printVel('down', 'yVel', drone.yVel);
            } else {
                drone.zVel -= 0.1;
                drone.zVel = Math.max(drone.zVel, -1);
                drone.updateZVel();
                printVel('back', 'zVel', drone.zVel);
            }
            break;
            
            case 'right':
            if(key.shift) {
                drone.xVel += 0.1;
                drone.xVel = Math.min(drone.xVel, 1);
                drone.updateXVel();
                printVel('strafe right', 'xVel', drone.xVel);
            } else {
                drone.rotationVel += 0.1;
                drone.rotationVel = Math.min(drone.rotationVel, 1);
                drone.updateRot();
                printVel('turn right', 'rotVel', drone.rotationVel);
            }
            break;
            
            case 'left':
            if(key.shift) {
                drone.xVel -= 0.1;
                drone.xVel = Math.max(drone.xVel, -1);
                drone.updateXVel()
                printVel('strafe left', 'xVel', drone.xVel);
            } else {
                drone.rotationVel -= 0.1;
                drone.rotationVel = Math.max(drone.rotationVel, -1);
                drone.updateRot();
                printVel('turn left', 'rotVel', drone.rotationVel);
            }
            break;
            
            case 's':
                drone.stop();
                console.log('stopping');
                break;
            case 'k':
                console.log('calibrating');
                client.calibrate(0);
                break;
        }
    }
});

drone.takeoff();
drone.update();

process.stdin.setRawMode(true);
process.stdin.resume();